#include "motion.h"

Motion::Motion(){
    _accelerometer = std::make_shared<Accelerometer>();
    _signalAnalyzer = std::make_shared<SignalAnalyzer>();
}

void Motion::start(){
    _accelerometer->Start();
}

bool Motion::HasNewMoves() const{
    return !_moves.empty();
}

void Motion::Stop(){
    _accelerometer->Stop();
}

void Motion::setMoves(std::vector<Move> mvs){
    _movesLock.lock();
    _moves = mvs;

    _movesLock.unlock();
}

std::vector<Move> Motion::ExtractAndClearMoves(){
    _movesLock.lock();
    auto copy = _moves;
    _moves.clear();
    _movesLock.unlock();
    return copy;
}