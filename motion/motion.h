#ifndef MOTION_H
#define MOTION_H

#include <memory>
#include "../accelerometer/Accelerometer.h"
#include "../signalanalyzer/SignalAnalyzer.h"
#include <mutex>

class Motion {
private:
    std::shared_ptr<Accelerometer> _accelerometer;
    std::shared_ptr<SignalAnalyzer> _signalAnalyzer;
    std::vector<Move> _moves;
    std::mutex _movesLock;

    void setMoves(std::vector<Move> mvs);

public:
    void start();
    void Stop();
    Motion();
    bool HasNewMoves() const;
    std::vector<Move> ExtractAndClearMoves();
};


#endif
