#include "Accelerometer.h"

Accelerometer::Accelerometer():_threadSleepMs(10){

}

void Accelerometer::Start(){
    _readThread = std::thread(&Accelerometer::_run, this);
}

void Accelerometer::_run() {
    _killThread = false;

    while (!_killThread){
        _updateData();
        std::this_thread::sleep_for(_threadSleepMs);
    }
}

void Accelerometer::Stop(){
    _killThread = true;
    if (_readThread.joinable())
        _readThread.join();
}

void Accelerometer::_updateData() {


}
