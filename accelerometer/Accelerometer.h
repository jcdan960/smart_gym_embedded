#ifndef ACCELEROMETER_H
#define ACCELEROMETER_H

#include <thread>
#include <vector>
#include <chrono>
#include "Axis.h"

struct{
    uint16_t x;
    uint16_t y;
    uint16_t z;
}AxisPort;

class Accelerometer {
private:
    bool _killThread;
    std::thread _readThread;
    std::vector<Axis> _axisReads;
    void _updateData();
    std::chrono::milliseconds _threadSleepMs;
    void _run();
public:
    Accelerometer();
    void Stop();
    void Start();
};


#endif
