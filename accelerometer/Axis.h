#ifndef AXIS_H
#define AXIS_H

#include <cstring>
struct Axis{
    float x;
    float y;
    float z;
    Axis(){
        memset(this,0.0,sizeof(this));
    }
    Axis(float x, float y, float z){
        this->x = x;
        this->y = y;
        this->z = z;
    }
};
#endif //AXIS_H
