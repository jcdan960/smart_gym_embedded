#ifndef SIGNALANALYZER_H
#define SIGNALANALYZER_H
#include <vector>
#include "../motion/move.h"
#include "../accelerometer/Axis.h"
#include "../motion/move.h"

class SignalAnalyzer {
public:
    static std::vector<Move> AnalyzeSignal(std::vector<Axis> &axisData);

};


#endif //SMART_GYM_EMBEDDED_SIGNALANALYZER_H
