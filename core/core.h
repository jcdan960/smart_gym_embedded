#ifndef CORE_H
#define CORE_H

#include <thread>
#include <memory>
#include "../motion/motion.h"
#include "../ISuscriber/ISuscriber.h"

class Core{
private:

    std::shared_ptr<Motion> _motion;
    bool _stopMainLoop;
    std::vector<std::shared_ptr<ISuscriber>> _suscribers;

public:
    Core();
    ~Core();
    void init();
    void MainLoop();
    void shutDown();
};

#endif //CORE_H
