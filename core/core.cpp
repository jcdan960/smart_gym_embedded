#include "core.h"

Core::Core():_stopMainLoop(true) {
    init();
}

Core::~Core(){
    shutDown();
}

void Core::init() {
    _motion = std::make_shared<Motion>();
}

void Core::MainLoop() {
    _motion->start();
    _stopMainLoop = false;

    while(!_stopMainLoop){
        if (_motion->HasNewMoves()){
            auto moves = _motion->ExtractAndClearMoves();
        }
    }

}

void Core::shutDown() {
    _motion->Stop();
    _stopMainLoop = true;
}

